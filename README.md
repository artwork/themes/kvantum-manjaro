## Kvantum config files for Manjaro

- **Adapta theme** ( *Maia* and *Breath* variation) 
- **Arc theme** ( *Maia* and *Breath* variation )
- **Matcha theme** ( *All* variation )

*Based on works of Papirus Development Team and Tsu Jan ( Kvantum developer )*

